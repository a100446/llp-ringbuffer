#ifndef RB_H
#define RB_H


typedef struct buffernode{
	int value;
	struct buffernode* next;
} node;

typedef struct ringbuffer{
    struct buffernode* head;
	struct buffernode* tail;
	int buffer_size;
} rb;


rb* init(int bs){
	rb* ring = (rb*)malloc(sizeof(rb));
	if(ring != NULL){
		ring->head = NULL;
		ring->tail = NULL;
		ring->buffer_size = bs;
	}
	return ring;
}

int count(rb* ring){
	if(ring == NULL)
		return -1;

	if(ring->head == NULL)
		return 0;

	node* current = ring->head;
	int counter = 0;
	while(current != NULL){
		//printf("%d\n", current->value);
		counter++;
		if(current == ring->tail)
			break;
		current = current->next;
	}
	return counter;
}

void printbuffer(rb* ring){
	int ring_size = count(ring);
	if(ring_size <= 0)
	{
		printf("Buffer is %s.\n", (ring_size == -1 ? "uninitialized" : "empty"));
		return;
	}

	printf("Buffer: %d/%d\n", count(ring), ring->buffer_size);

	node* current = ring->head;
	while(current != NULL){
		if(current == ring->head)
			printf("Buffer Start\nValues: ");

		printf("%d ", current->value);

		if(current == ring->tail){
			printf("\nBuffer End\n");
			break;
		}
		current = current->next;
	}
}

void add(rb* ring, int val){
	if (ring == NULL)
		return;
	node* new_node = (node*)malloc(sizeof(node));
	new_node->value = val;
	new_node->next = NULL;

	if (ring->head == NULL)//if buffer empty, set head and tail to new node
	{
		ring->head = new_node;
		ring->tail = new_node;
	}
	else if(ring->tail != NULL)//if buffer isn't empty
	{
		if(ring->tail->next == NULL)//if buffer hasnt reach its limit
		{
			ring->tail->next = new_node;
			ring->tail = new_node;
			if(count(ring) == ring->buffer_size)//if buffer did reach limit after new node
			{
				ring->tail->next = ring->head;
			}
		}
		else//if buffer has reached its limit
		{
			node* old_head = ring->head;
			ring->head = ring->head->next;
			new_node->next = ring->head;
			ring->tail->next = new_node;
			ring->tail = new_node;
			free(old_head);
		}
	}

	printf("Head: %d | ", ring->head->value);
	printf("Tail: %d | ", ring->tail->value);
	if(ring->tail->next != NULL)
		printf("Buffer full (%s)\n", (ring->tail->next == ring->head ? "Valid Ring" : "WARNING: INVALID RING"));
	else
		printf("Buffer not full\n");
	
}

void freebuffer(rb* ring){
	if(ring == NULL || ring->head == NULL)
		return;

	node* current = ring->head;
	ring->tail->next = NULL;
	printf("Freed Values: ");
	while(current != NULL)
	{
		node* old = current;
		current = current->next;
		printf("%d ", old->value);
		free(old);
	}
	free(ring);//should we free the ring as well here?
	printf("\n");
}


#endif