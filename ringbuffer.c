#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ringbuffer.h"

int main(int argc, char *argv[]) 
{
	printbuffer(NULL);
	freebuffer(NULL);

	rb* ring = init(10);

	printbuffer(ring);
	freebuffer(ring);

	for(int i = 1; i <= 20; i++){
		add(ring, i);
	}
	
	printbuffer(ring);

	freebuffer(ring);

	return 0;
}
